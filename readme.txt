http://www.deakin.edu.au/~cdle/sit780ass2

This system is built by using
	CodeIgniter framework (https://www.codeigniter.com/)
	AngularJs (https://angularjs.org/)
	Bootstrap (http://getbootstrap.com/)
	jQuery (https://jquery.com/)
	Tether (http://tether.io/)
	Font Awesome (http://fontawesome.io/)