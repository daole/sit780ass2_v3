<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff_XML extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $staff = array();
        $staffXML = $this->load();
        $employees = $staffXML->getElementsByTagName('employee');
        foreach($employees as $employeeXML) {
            $employee = $this->parseEmployeeData($employeeXML);
            $staff[] = $employee;
        }
        return $staff;
    }

    public function get_staff_by_surname_or_given_name($input) {
        $staff = array();
        $staffXML = $this->load();
        $xpath = new DOMXpath($staffXML);
        $employees = $xpath->query('/staff/employee/surname[contains(., "'.$input['search'].'")]/parent::*|/staff/employee/given_name[contains(., "'.$input['search'].'")]/parent::*');
        foreach($employees as $employeeXML) {
            $employee = $this->parseEmployeeData($employeeXML);
            $staff[] = $employee;
        }
        return $staff;
    }

    public function insert($input) {
        $staffXML = $this->load();
        $staff = $staffXML->getElementsByTagName('staff');
        $staff = $staff->item(0);
        $nextId = (int)$staff->getAttribute('next_id') + 1;
        $staff->setAttribute('next_id', $nextId);
        $employee = $staffXML->createElement('employee');
        $staffId = $staffXML->createElement('staff_id', $nextId);
        $staffId->setAttribute('email', $input['email']);
        $employee->appendChild($staffId);
        $surname = $staffXML->createElement('surname', $input['surname']);
        $employee->appendChild($surname);
        $givenName = $staffXML->createElement('given_name', $input['given_name']);
        $employee->appendChild($givenName);
        $address = $staffXML->createElement('address', $input['address']);
        $employee->appendChild($address);
        $staff->appendChild($employee);
        $this->save($staffXML);
    }

    public function delete($input) {
        $staffXML = $this->load();
        $xpath = new DOMXpath($staffXML);
        $employees = $xpath->query('//staff/employee/staff_id[.="'.$input['id'].'"]/parent::*');
        foreach($employees as $employee) {
            $employee->parentNode->removeChild($employee);
        }
        $this->save($staffXML);
    }

    private function load() {
        $staffXML = new DOMDocument();
        $staffXML->load(PATH_XML.'/staff.xml');
        return $staffXML;
    }

    private function parseEmployeeData($employeeXML) {
        $employee = array();
        foreach ($employeeXML->childNodes as $childNode) {
            switch($childNode->nodeName) {
                case 'staff_id':
                    $employee['staff_id'] = $childNode->nodeValue;
                    $employee['email'] = $childNode->getAttribute('email');
                    break;
                case 'surname':
                    $employee['surname'] = $childNode->nodeValue;
                    break;
                case 'given_name':
                    $employee['given_name'] = $childNode->nodeValue;
                    break;
                case 'address':
                    $employee['address'] = $childNode->nodeValue;
                    break;
            }
        }
        return $employee;
    }

    private function save($staffXML) {
        $staffXML->save(PATH_XML.'/staff.xml');
    }
}