<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_Login extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    public function captcha() {
        $this->load->helper('captcha');
        $configuration = array(
            'img_path' => PATH_PUBLIC.'/images/captcha/',
            'img_url' => "/public/images/captcha/",
            'font_path' => PATH_PUBLIC.'/fonts/digit.ttf',
            'img_width' => 150,
            'img_height' => 50,
            'expiration' => 300, // 5 minutes
            'word_length' => 5,
            'font_size' => 20,
            'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
                'background' => array(0, 0, 0),
                'border' => array(0, 255, 0),
                'text' => array(0, 255, 0),
                'grid' => array(0, 0, 255)
            )
        );
        $captcha = create_captcha($configuration);
        return $captcha;
    }

    public function get_all() {
        $sql = '
          SELECT
            user_login.id,
            user_login.username,
            user_login.role
          FROM
            user_login
          ORDER BY
            user_login.id
        ';
        $result = $this->db->query($sql);
        if($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_user_by_username($input) {
        $sql = '
          SELECT
            user_login.id,
            user_login.username,
            user_login.role
          FROM
            user_login
          WHERE
            user_login.username = ?
        ';
        $result = $this->db->query($sql, array($input['username']));
        if($result->num_rows() > 0) {
            $resultArray = $result->result_array();
            return $resultArray[0];
        }
        return null;
    }

    public function get_user_by_username_and_password($input) {
        $sql = '
          SELECT
            user_login.id,
            user_login.username,
            user_login.role
          FROM
            user_login
          WHERE
            user_login.username = ?
            and user_login.password = ?
        ';
        $result = $this->db->query($sql, array($input['username'], md5($input['password'])));
        if($result->num_rows() > 0) {
            $resultArray = $result->result_array();
            return $resultArray[0];
        }
        return null;
    }

    public function insert($input) {
        $sql = '
          INSERT INTO
            user_login(
              user_login.username,
              user_login.password,
              user_login.role
            )
          VALUES(
            ?,
            ?,
            ?
          )
        ';
        $this->db->query($sql, array($input['username'], md5($input['password']), $input['role']));
        return $this->db->affected_rows();
    }

    public function delete($input) {
        $sql = '
          DELETE
          FROM
            user_login
          WHERE
            user_login.id = ?
        ';
        $this->db->query($sql, array($input['id']));
        return $this->db->affected_rows();
    }
}