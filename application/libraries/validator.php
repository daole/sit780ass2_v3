<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Validator {
    private $CI;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->load->library('validation');
    }

    public function validate_login_input($input) {
        $validationOptions = array();

        $validationOption = array();
        $validationOption['itemName'] = 'Username';
        $validationOption['htmlItemId'] = 'username';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['username'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Password';
        $validationOption['htmlItemId'] = 'password';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['password'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Captcha';
        $validationOption['htmlItemId'] = 'captcha';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY, Validation::IS_SAME_TO);
        $validationOption['data'] = $input['captcha'];
        $validationOption['equal'] = $this->CI->session->userdata('captchaWord');
        $validationOption['errMsg'][Validation::IS_SAME_TO] = CUSTOM_ERROR_MESSAGE_CAPTCHA_INCORRECT;
        $validationOptions[] = $validationOption;

        $this->CI->validation->is_validate_multiple($validationOptions);
        $errors = $this->CI->validation->get_errors();
        return $errors;
    }

    public function validate_new_user_input($input) {
        $validationOptions = array();

        $validationOption = array();
        $validationOption['itemName'] = 'Username';
        $validationOption['htmlItemId'] = 'username';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['username'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Password';
        $validationOption['htmlItemId'] = 'password';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['password'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Confirmation password';
        $validationOption['htmlItemId'] = 'confirmation_password';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY, Validation::IS_SAME_TO);
        $validationOption['data'] = $input['confirmation_password'];
        $validationOption['equal'] = $input['password'];
        $validationOption['errMsg'][Validation::IS_SAME_TO] = CUSTOM_ERROR_MESSAGE_CONFIRMATION_PASSWORD_MISMATCH;
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Role';
        $validationOption['htmlItemId'] = 'role';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY, Validation::IS_IN_ARRAY);
        $validationOption['data'] = $input['role'];
        $validationOption['array'] = array(ROLE_ADMIN, ROLE_USER);
        $validationOptions[] = $validationOption;

        $this->CI->validation->is_validate_multiple($validationOptions);
        $errors = $this->CI->validation->get_errors();
        return $errors;
    }

    public function validate_new_staff_input($input) {
        $validationOptions = array();

        $validationOption = array();
        $validationOption['itemName'] = 'Email';
        $validationOption['htmlItemId'] = 'email';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY, Validation::IS_EMAIL_ADDRESS);
        $validationOption['data'] = $input['email'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Surname';
        $validationOption['htmlItemId'] = 'surname';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['surname'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Given name';
        $validationOption['htmlItemId'] = 'given_name';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['given_name'];
        $validationOptions[] = $validationOption;

        $validationOption = array();
        $validationOption['itemName'] = 'Address';
        $validationOption['htmlItemId'] = 'address';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY);
        $validationOption['data'] = $input['address'];
        $validationOptions[] = $validationOption;

        $this->CI->validation->is_validate_multiple($validationOptions);
        $errors = $this->CI->validation->get_errors();
        return $errors;
    }

    public function validate_delete_input($input) {
        $validationOptions = array();

        $validationOption = array();
        $validationOption['itemName'] = 'Id';
        $validationOption['htmlItemId'] = 'id';
        $validationOption['validationTypes'] = array(Validation::IS_NOT_EMPTY, Validation::IS_NUMBER);
        $validationOption['data'] = $input['id'];
        $validationOptions[] = $validationOption;

        $this->CI->validation->is_validate_multiple($validationOptions);
        $errors = $this->CI->validation->get_errors();
        return $errors;
    }
}