<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Validation {
    const IS_NUMBER = 1;
    const IS_NOT_EMPTY = 2;
    const IS_ALPHABET = 3;
    const IS_ALPHABET_NUMBER = 4;
    const IS_FILE = 5;
    const IS_EMAIL_ADDRESS = 6;
    const IS_LONGER_THAN = 7;
    const IS_SHORTER_THAN = 8;
    const IS_LENGTH_BETWEEN = 9;
    const IS_LENGTH_EQUAL = 10;
    const IS_SAME_TO = 11;
    const IS_POSTCODE = 12;
    const IS_PHONE_NUMBER = 13;
    const IS_NOT_SAME_TO = 14;
    const IS_DATE = 15;
    const IS_SMALLER_THAN = 16;
    const IS_GREATER_THAN = 17;
    const IS_BETWEEN = 18;
    const IS_ALPHABET_NUMBER_MARK = 19;
    const IS_DECIMAL = 20;
    const IS_AFTER = 21;
    const IS_BEFORE = 22;
    const IS_DATETIME_BETWEEN = 23;
    const IS_IN_ARRAY = 24;
    const IS_BOOL = 25;
    const IS_VERSION = 26;
    const IS_DATETIME = 27;

    private static $DATE_FORMAT = 'Y-m-d';
    private static $DATETIME_FORMAT = 'Y-m-d H:i:s';

    private $errors = array();

    public function is_error() {
        return count($this->errors) > 0;
    }

    public function get_errors() {
        return $this->errors;
    }

    public function add_error($errorMessage, $htmlItemId) {
        $this->errors[$htmlItemId][] = $errorMessage;
    }

    public function format_message($message, $arg = array()) {
        for ($i = 0; $i < count($arg); $i++) {
            $message = str_replace("{" . $i . "}", $arg[$i], $message);
        }
        return $message;
    }

    public function is_validate_multiple($opts) {
        foreach ($opts as $value) {
            $this->is_validate(
                $value["itemName"],
                $value["htmlItemId"],
                $value["data"],
                $value["validationTypes"],
                isset($value["min"]) ? $value["min"] : null,
                isset($value["max"]) ? $value["max"] : null,
                isset($value["equal"]) ? $value["equal"] : null,
                isset($value["array"]) ? $value["array"] : null,
                isset($value["errMsg"]) ? $value["errMsg"] : null
            );
        }
    }

    public function is_validate($itemName, $htmlItemId, $data, $validationTypes, $min = null, $max = null, $equal = null, $array = null, $errMsg = null) {
        if (is_array($validationTypes)) {
            foreach ($validationTypes as $num) {
                switch ($num) {
                    case self::IS_NOT_EMPTY:
                        if ($this->is_empty($data)) {
                            if (isset($errMsg[self::IS_NOT_EMPTY])) {
                                $this->add_error($errMsg[self::IS_NOT_EMPTY], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_NOT_EMPTY, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_BOOL:
                        if (!$this->is_boolean($data)) {
                            if (isset($errMsg[self::IS_BOOL])) {
                                $this->add_error($errMsg[self::IS_BOOL], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_BOOL, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_NUMBER:
                        if (!$this->is_number($data)) {
                            if (isset($errMsg[self::IS_NUMBER])) {
                                $this->add_error($errMsg[self::IS_NUMBER], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_NUMBER, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_DECIMAL:
                        if (!$this->is_decimal($data)) {
                            if (isset($errMsg[self::IS_DECIMAL])) {
                                $this->add_error($errMsg[self::IS_DECIMAL], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_DECIMAL, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_GREATER_THAN:
                        if (!$this->is_greater_than($data, $min)) {
                            if (isset($errMsg[self::IS_GREATER_THAN])) {
                                $this->add_error($errMsg[self::IS_GREATER_THAN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_GREATER_THAN, array($itemName, $min)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_SMALLER_THAN:
                        if (!$this->is_smaller_than($data, $max)) {
                            if (isset($errMsg[self::IS_SMALLER_THAN])) {
                                $this->add_error($errMsg[self::IS_SMALLER_THAN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_SMALLER_THAN, array($itemName, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_BETWEEN:
                        if (!$this->is_between($data, $min, $max)) {
                            if (isset($errMsg[self::IS_BETWEEN])) {
                                $this->add_error($errMsg[self::IS_BETWEEN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_BETWEEN, array($itemName, $min, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_ALPHABET:
                        if (!$this->is_alphabet($data)) {
                            if (isset($errMsg[self::IS_ALPHABET])) {
                                $this->add_error($errMsg[self::IS_ALPHABET], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_ALPHABET, array($itemName)), $htmlItemId);
                            }
                        }
                        break;

                    case self::IS_ALPHABET_NUMBER:
                        if (!$this->is_alphabet_number($data)) {
                            if (isset($errMsg[self::IS_ALPHABET_NUMBER])) {
                                $this->add_error($errMsg[self::IS_ALPHABET_NUMBER], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_ALPHABET_NUMBER, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_ALPHABET_NUMBER_MARK:
                        if (!$this->is_alphabet_number_mark($data)) {
                            if (isset($errMsg[self::IS_ALPHABET_NUMBER_MARK])) {
                                $this->add_error($errMsg[self::IS_ALPHABET_NUMBER_MARK], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_ALPHABET_NUMBER_MARK, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_EMAIL_ADDRESS:
                        if (!$this->is_email_address($data)) {
                            if (isset($errMsg[self::IS_EMAIL_ADDRESS])) {
                                $this->add_error($errMsg[self::IS_EMAIL_ADDRESS], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_EMAIL_ADDRESS, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_LONGER_THAN:
                        if (!$this->is_longer_than($data, $min)) {
                            if (isset($errMsg[self::IS_LONGER_THAN])) {
                                $this->add_error($errMsg[self::IS_LONGER_THAN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_LONGER_THAN, array($itemName, $min)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_SHORTER_THAN:
                        if (!$this->is_shorter_than($data, $max)) {
                            if (isset($errMsg[self::IS_SHORTER_THAN])) {
                                $this->add_error($errMsg[self::IS_SHORTER_THAN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_SHORTER_THAN, array($itemName, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_LENGTH_BETWEEN:
                        if (!$this->is_length_between($data, $min, $max)) {
                            if (isset($errMsg[self::IS_LENGTH_BETWEEN])) {
                                $this->add_error($errMsg[self::IS_LENGTH_BETWEEN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_LENGTH_BETWEEN, array($itemName, $min, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_LENGTH_EQUAL:
                        if (!$this->is_length_equal($data, $equal)) {
                            if (isset($errMsg[self::IS_LENGTH_EQUAL])) {
                                $this->add_error($errMsg[self::IS_LENGTH_EQUAL], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_LENGTH_EQUAL, array($itemName, $equal)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_SAME_TO:
                        if (!$this->is_same_to($data, $equal)) {
                            if (isset($errMsg[self::IS_SAME_TO])) {
                                $this->add_error($errMsg[self::IS_SAME_TO], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_SAME_TO, array($itemName, $equal)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_NOT_SAME_TO:
                        if (!$this->is_not_same_to($data, $equal)) {
                            if (isset($errMsg[self::IS_NOT_SAME_TO])) {
                                $this->add_error($errMsg[self::IS_NOT_SAME_TO], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_NOT_SAME_TO, array($itemName, $equal)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_POSTCODE:
                        if (!$this->is_postcode($data)) {
                            if (isset($errMsg[self::IS_POSTCODE])) {
                                $this->add_error($errMsg[self::IS_POSTCODE], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_POSTCODE, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_PHONE_NUMBER:
                        if (!$this->is_phone_number($data)) {
                            if (isset($errMsg[self::IS_PHONE_NUMBER])) {
                                $this->add_error($errMsg[self::IS_PHONE_NUMBER], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_PHONE_NUMBER, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_DATE:
                        if (!$this->is_date($data)) {
                            if (isset($errMsg[self::IS_DATE])) {
                                $this->add_error($errMsg[self::IS_DATE], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_DATE, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_VERSION:
                        if (!$this->is_version($data)) {
                            if (isset($errMsg[self::IS_VERSION])) {
                                $this->add_error($errMsg[self::IS_VERSION], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_VERSION, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_AFTER:
                        if (!$this->is_after($data, $min)) {
                            if (isset($errMsg[self::IS_AFTER])) {
                                $this->add_error($errMsg[self::IS_AFTER], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_AFTER, array($itemName, $min)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_BEFORE:
                        if (!$this->is_before($data, $max)) {
                            if (isset($errMsg[self::IS_BEFORE])) {
                                $this->add_error($errMsg[self::IS_BEFORE], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_BEFORE, array($itemName, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_DATETIME_BETWEEN:
                        if (!$this->is_datetime_between($data, $min, $max)) {
                            if (isset($errMsg[self::IS_DATETIME_BETWEEN])) {
                                $this->add_error($errMsg[self::IS_DATETIME_BETWEEN], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_DATETIME_BETWEEN, array($itemName, $min, $max)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_FILE:
                        if (!$this->is_file($data)) {
                            if (isset($errMsg[self::IS_FILE])) {
                                $this->add_error($errMsg[self::IS_FILE], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_FILE, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_IN_ARRAY:
                        if (!$this->is_in_array($data, $array)) {
                            if (isset($errMsg[self::IS_IN_ARRAY])) {
                                $this->add_error($errMsg[self::IS_IN_ARRAY], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_IN_ARRAY, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                    case self::IS_DATETIME:
                        if (!$this->is_datetime($data)) {
                            if (isset($errMsg[self::IS_DATETIME])) {
                                $this->add_error($errMsg[self::IS_DATETIME], $htmlItemId);
                            } else {
                                $this->add_error($this->format_message(ERROR_MESSAGE_IS_DATETIME, array($itemName)), $htmlItemId);
                            }
                        }
                        break;
                }
            }
        }
    }

    public function is_number($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match('/^[0-9]+$/', $data)) {
                $result = false;
            }
        }
        return $result;
    }


    public function is_alphabet($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match('/^[a-zA-Z]+$/', $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_alphabet_number($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match('/^[a-zA-Z0-9]+$/', $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_alphabet_number_mark($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match('/^[a-zA-Z0-9\-\+]+$/', $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_empty($data) {
        if (is_null($data)) {
            return true;
        } else {
            if ($data == '') {
                return true;
            }
        }
        return false;
    }

    public function is_file($filepath) {
        $result = true;
        if (!file_exists($filepath)) {
            $result = false;
        }
        return $result;
    }

    public function is_email_address($email) {
        $result = true;
        if (!$this->is_empty($email)) {
            if (!preg_match('/^([\w])+([\w\._-])*\@([\w])+([\w\._-])*\.([a-zA-Z])+$/', $email)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_greater_than($data, $min) {
        $result = true;
        if (!$this->is_empty($data)) {
            if ($data < $min) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_smaller_than($data, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if ($data > $max) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_between($data, $min, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!($this->is_smaller_than($data, $max) && $this->is_greater_than($data, $min))) {
                $result = false;
            }
        }
        return $result;
    }


    public function is_shorter_than($data, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (strlen($data) > $max) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_longer_than($data, $min) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (strlen($data) < $min) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_length_between($data, $min, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!($this->is_shorter_than($data, $max) && $this->is_longer_than($data, $min))) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_length_equal($data, $eq) {
        $result = true;
        if (strlen($data) != $eq) {
            $result = false;
        }
        return $result;
    }

    public function is_same_to($data, $eq) {
        $result = true;
        if ($data != $eq) {
            $result = false;
        }
        return $result;
    }

    public function is_not_same_to($data, $eq) {
        $result = true;
        if ($data == $eq) {
            $result = false;
        }
        return $result;
    }

    public function is_postcode($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match("/^[0-9]{3}-[0-9]{4}$/", $data)) {//old: /^\d{3}\-\d{4}$/
                $result = false;
            }
        }
        return $result;
    }

    public function is_phone_number($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match("/^[0-9]{2,4}-?[0-9]{2,4}-?[0-9]{3,4}$/", $data)) {//old: /^\d{2,4}-\d{2,4}-\d{4}$/
                $result = false;
            }
        }
        return $result;
    }

    public function is_date($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            $date = DateTime::createFromFormat(self::$DATE_FORMAT, $data);
            //$date = $this->parse_date($data);
            if (!($date && $date->format(self::$DATE_FORMAT) == $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_before($data, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if ($this->is_date($data) && $this->is_date($max)) {
                $dataDate = DateTime::createFromFormat(self::$DATE_FORMAT, $data);
                $maxDate = DateTime::createFromFormat(self::$DATE_FORMAT, $max);
                //$dataDate = $this->parse_date($data);
                //$maxDate = $this->parse_date($max);
                if ($dataDate > $maxDate) {
                    $result = false;
                }
            } else {
                $result = false;
            }
        }
        return $result;
    }

    public function is_after($data, $min) {
        $result = true;
        if (!$this->is_empty($data)) {
            if ($this->is_date($data) && $this->is_date($min)) {
                $dataDate = DateTime::createFromFormat(self::$DATE_FORMAT, $data);
                $minDate = DateTime::createFromFormat(self::$DATE_FORMAT, $min);
                //$dataDate = $this->parse_date($data);
                //$minDate = $this->parse_date($min);
                if ($dataDate < $minDate) {
                    $result = false;
                }
            } else {
                $result = false;
            }
        }
        return $result;
    }

    public function is_datetime_between($data, $min, $max) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!($this->is_after($data, $min) && $this->is_before($data, $max))) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_decimal($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match("/^[0-9]+(\.[0-9]+)?$/", $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_version($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!preg_match("/^[0-9]+(\.[0-9]+)+$/", $data)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_boolean($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            $result = is_bool($data);
        }
        return $result;
    }

    public function is_in_array($data, $array) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (!in_array($data, $array)) {
                $result = false;
            }
        }
        return $result;
    }

    public function is_datetime($data) {
        $result = true;
        if (!$this->is_empty($data)) {
            if (date(self::$DATETIME_FORMAT, strtotime($data)) != $data) {
                $result = false;
            }
        }
        return $result;
    }

    public function parse_date($data) {
        if(!$data) {
            return false;
        }
        $data = trim($data );
        $dateParts = explode('-', $data );
        $datePartsCount = count($dateParts );
        for( $i = 0; $i < $datePartsCount; $i++ ) {
            if (!$this->is_number($dateParts[$i])) {
                return false;
            }
        }
        $year = $dateParts[0];
        $month = $dateParts[1];
        $day = $dateParts[2];
        if(checkdate($month, $day, $year) ){
            $date = new DateTime();
            $date->setDate($year, $month, $day);
            return $date;
        } else {
            return false;
        }
    }
}