<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'core/REST_Controller.php';
class MY_Controller extends REST_Controller {
    function __construct($requireLogin, $requireAdminPrivilege) {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('validator');
        if($requireLogin || $requireAdminPrivilege) {
            if(!$this->isLoggedIn()) {
                $this->returnUnautorizedError();
            }
        }
        if($requireAdminPrivilege) {
            if(!$this->isAdmin()) {
                $this->returnNotEnoughPrivilegeError();
            }
        }
    }

    protected function isLoggedIn() {
        return $this->session->userdata('user') ? true : false;
    }

    protected function isAdmin() {
        $user = $this->session->userdata('user');
        return $user && $user['ROLE'] == ROLE_ADMIN ? true : false;
    }

    protected function returnUnautorizedError() {
        $this->response(array(KEY_ERROR_CODE => ERROR_CODE_UNAUTHORIZED), 401);
        exit;
    }

    protected function returnNotEnoughPrivilegeError() {
        $this->response(array(KEY_ERROR_CODE => ERROR_CODE_NOT_ENOUGH_PRIVILEGE), 401);
        exit;
    }
}