<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('ERROR_MESSAGE_IS_NUMBER', '{0} is not a number.');
define('ERROR_MESSAGE_IS_NOT_EMPTY', '{0} could not be left blank.');
define('ERROR_MESSAGE_IS_ALPHABET', '{0} must contain only alphabets.');
define('ERROR_MESSAGE_IS_ALPHABET_NUMBER', '{0} must contain alphabets and/or numbers.');
define('ERROR_MESSAGE_IS_FILE', '{0} is not a file.');
define('ERROR_MESSAGE_IS_EMAIL_ADDRESS', '{0} is not a valid email address.');
define('ERROR_MESSAGE_IS_LONGER_THAN', '{0} must be at least {1} character(s).');
define('ERROR_MESSAGE_IS_SHORTER_THAN', '{0} must not exceed {1} character(s).');
define('ERROR_MESSAGE_IS_LENGTH_BETWEEN', '{0} must be between {1} and {2} characters.');
define('ERROR_MESSAGE_IS_LENGTH_EQUAL', '{0} must be exactly {1} character(s).');
define('ERROR_MESSAGE_IS_SAME_TO', '{0} is not same to {1}.');
define('ERROR_MESSAGE_IS_POSTCODE', '{0} is not a valid postcode.');
define('ERROR_MESSAGE_IS_PHONE_NUMBER', '{0} is not a valid telephone number.');
define('ERROR_MESSAGE_IS_NOT_SAME_TO', '{0} is should not be same to {1}.');
define('ERROR_MESSAGE_IS_DATE', '{0} is not a valid date.');
define('ERROR_MESSAGE_IS_DATETIME', '{0} is not a valid datetime.');
define('ERROR_MESSAGE_IS_SMALLER_THAN', '{0} must not be greater than {1}.');
define('ERROR_MESSAGE_IS_GREATER_THAN', '{0} must be at least {1}.');
define('ERROR_MESSAGE_IS_BETWEEN', '{0} must be between {1} and {2}.');
define('ERROR_MESSAGE_IS_ALPHABET_NUMBER_MARK', '{0} must contain only alphabets and/or numbers plus mark sign.');
define('ERROR_MESSAGE_IS_DECIMAL', '{0} is not a decimal.');
define('ERROR_MESSAGE_IS_AFTER', '{0} must be after {1}.');
define('ERROR_MESSAGE_IS_BEFORE', '{0} must be before {1}.');
define('ERROR_MESSAGE_IS_DATETIME_BETWEEN', '{0} must be between {1} and {2}.');
define('ERROR_MESSAGE_IS_IN_ARRAY', 'Please select a valid option for {0}.');
define('ERROR_MESSAGE_IS_BOOL', '{0} is not a valid value.');
define('ERROR_MESSAGE_IS_VERSION', '{0} is not a valid version number.');

define('CUSTOM_ERROR_MESSAGE_CAPTCHA_INCORRECT', 'Captcha is incorrect.');
define('CUSTOM_ERROR_MESSAGE_LOGIN_FAIL', 'Username or password is incorrect.');
define('CUSTOM_ERROR_MESSAGE_CONFIRMATION_PASSWORD_MISMATCH', 'Password and confirmation password do not match.');
define('CUSTOM_ERROR_MESSAGE_USERNAME_ALREADY_EXISTS', 'Username already exists.');

define('ERROR_CODE_UNAUTHORIZED', 1);
define('ERROR_CODE_NOT_ENOUGH_PRIVILEGE', 2);
define('ERROR_CODE_INPUT_ERROR', 3);

define('KEY_ERROR_CODE', 'errorCode');
define('KEY_ERRORS', 'errors');
define('KEY_CUSTOM_ERROR', 'custom');
define('KEY_MESSAGE', 'message');