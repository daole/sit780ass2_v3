<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title>SIT780/Assignment 2</title>
		<link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/css/tether.min.css"/>
		<link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/css/bootstrap.min.css"/>
		<link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/css/font-awesome.min.css"/>
		<link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/css/app.css"/>
		<script type="text/javascript">
			var baseURL = '<?= base_url(); ?>';
			<?php if($user) { ?>
				var currentUser = <?= json_encode($user) ?>;
			<?php } ?>
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXGWoAFxNKLhaoPObUMGuG2Z5KfYjqHq4" async defer></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/jquery-3.2.0.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/tether.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/angular.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/angular-resource.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>public/js/app.js"></script>
	</head>
	<body>
		<div id="app" ng-app="app">
			<div id="place_holder_main" ui-view></div>
		</div>
	</body>
</html>