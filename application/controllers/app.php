<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper('json');
        $this->load->library('session');
    }

	public function index() {
	    $user = $this->session->userdata('user');
		$this->load->view('app', array('user' => $user));
	}
}
