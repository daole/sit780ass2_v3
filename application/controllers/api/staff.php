<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff extends MY_Controller {
    function __construct() {
        parent::__construct(true, false);
        $this->load->model('staff_xml', 'staffXML');
    }

    function index_get() {
        $input['search'] = $this->input->get('search');
        if(empty($input['search'])) {
            $staff = $this->staffXML->get_all();
        } else {
            $staff = $this->staffXML->get_staff_by_surname_or_given_name($input);
        }
        $this->response($staff, 200);
    }

    function index_post() {
        if(!$this->isAdmin()) {
            $this->returnNotEnoughPrivilegeError();
        }
        $json = $this->retrieve_json();
        $input = array();
        $input['email'] = isset($json->email) ? $json->email : null;
        $input['surname'] = isset($json->surname) ? $json->surname : null;
        $input['given_name'] = isset($json->given_name) ? $json->given_name : null;
        $input['address'] = isset($json->address) ? $json->address : null;
        $errors = $this->validator->validate_new_staff_input($input);
        if(count($errors) > 0) {
            $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors), 400);
        } else {
            $this->staffXML->insert($input);
            $this->response(array('result' => 'success'), 201);
        }
    }

    function index_delete() {
        if(!$this->isAdmin()) {
            $this->returnNotEnoughPrivilegeError();
        }
        $input['id'] = $this->input->get('id');
        $errors = $this->validator->validate_delete_input($input);
        if(count($errors) > 0) {
            $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors), 400);
        } else {
            $this->staffXML->delete($input);
            $this->response(array('result' => 'success'), 204);
        }
    }
}