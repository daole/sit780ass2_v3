<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends MY_Controller {
    function __construct() {
        parent::__construct(true, true);
        $this->load->model('user_login', 'userLogin');
    }

    function index_get() {
        $users = $this->userLogin->get_all();
        $this->response($users, 200);
    }

    function index_post() {
        $json = $this->retrieve_json();
        $input = array();
        $input['username'] = isset($json->username) ? $json->username : null;
        $input['password'] = isset($json->password) ? $json->password : null;
        $input['confirmation_password'] = isset($json->confirmation_password) ? $json->confirmation_password : null;
        $input['role'] = isset($json->role) ? $json->role : null;
        $errors = $this->validator->validate_new_user_input($input);
        if(count($errors) > 0) {
            $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors), 400);
        } else {
            $user = $this->userLogin->get_user_by_username($input);
            if($user) {
                $errors[KEY_CUSTOM_ERROR][] = CUSTOM_ERROR_MESSAGE_USERNAME_ALREADY_EXISTS;
                $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors), 400);
            } else {
                $this->userLogin->insert($input);
                $this->response(array('result' => 'success'), 201);
            }
        }
    }

    function index_delete() {
        $input['id'] = $this->input->get('id');
        $errors = $this->validator->validate_delete_input($input);
        if(count($errors) > 0) {
            $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors), 400);
        } else {
            $this->userLogin->delete($input);
            $this->response(array('result' => 'success'), 204);
        }
    }
}