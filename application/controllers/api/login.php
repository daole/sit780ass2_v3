<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {
    function __construct() {
        parent::__construct(false, false);
        $this->load->model('user_login', 'userLogin');
    }

    public function captcha_get() {
        $captcha = $this->getCaptcha();
        $this->response(array('filename' => $captcha['filename']), 200);
    }

    public function index_post() {
        $json = $this->retrieve_json();
        $input = array();
        $input['username'] = isset($json->username) ? $json->username : null;
        $input['password'] = isset($json->password) ? $json->password : null;
        $input['captcha'] = isset($json->captcha) ? $json->captcha : null;
        $errors = $this->validator->validate_login_input($input);
        if(count($errors) > 0) {
            $captcha = $this->getCaptcha();
            $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors, 'captcha' => array('filename' => $captcha['filename'])), 400);
        } else {
            $user = $this->userLogin->get_user_by_username_and_password($input);
            if($user) {
                $this->session->set_userdata('user', $user);
                $this->response(array('user' => $user), 200);
            } else {
                $errors[KEY_CUSTOM_ERROR][] = CUSTOM_ERROR_MESSAGE_LOGIN_FAIL;
                $captcha = $this->getCaptcha();
                $this->response(array(KEY_ERROR_CODE => ERROR_CODE_INPUT_ERROR, KEY_ERRORS => $errors, 'captcha' => array('filename' => $captcha['filename'])), 400);
            }
        }
    }

    private function getCaptcha() {
        $captcha = $this->userLogin->captcha();
        $this->session->set_userdata('captchaWord', $captcha['word']);
        return $captcha;
    }
}