<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends MY_Controller {
    function __construct() {
        parent::__construct(true, false);
    }

    public function index_post() {
        $this->session->unset_userdata('user');
        $this->response(array('user' => null), 204);
    }
}