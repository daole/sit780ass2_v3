var app = angular.module('app', ['ngResource', 'ui.router']);

app.config(['$windowProvider', '$stateProvider', '$urlRouterProvider', function($windowProvider, $stateProvider, $urlRouterProvider) {
	var $window = $windowProvider.$get();
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: $window.baseURL + 'public/html/pages/login.html',
            controller: 'LoginController',
            data: {
                requireLogin: false
            }
        })
        .state('panel', {
            abstract: true,
            url: '/panel',
            templateUrl: $window.baseURL + 'public/html/pages/panel.html',
            data: {
                requireLogin: true
            }
        })
        .state('panel.home', {
            url: '/home',
            templateUrl: $window.baseURL + 'public/html/pages/home.html',
            controller: 'HomeController'
        })
        .state('panel.staff', {
            url: '/staff',
            templateUrl: $window.baseURL + 'public/html/pages/staff.html',
            controller: 'StaffController'
        })
        .state('panel.users', {
            url: '/users',
            templateUrl: $window.baseURL + 'public/html/pages/users.html',
            controller: 'UsersController'
        })
        .state('panel.logout', {
            url: '/logout',
            controller: 'LogoutController'
        });
    $urlRouterProvider.otherwise('/login');
}]);

app.run(['$window', '$rootScope', '$state', function ($window, $rootScope, $state) {
    if($window.currentUser) {
        $rootScope.currentUser = $window.currentUser;
    }
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;
        if ((requireLogin && !$rootScope.currentUser)) {
            event.preventDefault();
            $state.go('login');
        }
    });
}]);

app.controller('LoginController', ['$window', '$rootScope', '$scope', '$state', 'LoginAPI', function($window, $rootScope, $scope, $state, LoginAPI) {
    var getCaptcha = function() {
        $scope.captcha = LoginAPI.captcha();
    }

    var changeState = function() {
        $state.go('panel.home');
    }

    var clearLoginForm = function() {
        $scope.login.username = '';
        $scope.login.password = '';
        $scope.login.captcha = '';
    }

    $scope.changeCaptcha = function() {
        getCaptcha();
    }

    $scope.logIn = function() {
        $scope.login.$login(function(response) {
            $rootScope.currentUser = response.user;
            changeState();
        }, function(response) {
            $scope.updateResult = response.data;
            clearLoginForm();
            $scope.captcha = $scope.updateResult.captcha;
        });
    }

    if($rootScope.currentUser) {
        changeState();
        return;
    }
	$scope.window = $window;
    $scope.login = new LoginAPI();
    getCaptcha();
}]);

app.controller('HomeController', ['$scope', function($scope) {
}]);

app.controller('StaffController', ['$scope', 'StaffAPI', function($scope, StaffAPI) {
    var addStaffModal = $('#modal_add_staff');
    var deleteStaffModal = $('#modal_delete_staff');
    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map($('#map')[0], {
        zoom: 15,
        center: new google.maps.LatLng(0, 0),
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var getStaff = function() {
        $scope.staff = StaffAPI.query({
            search: $scope.searchValue ? $scope.searchValue : ''
        });
    }

    $scope.search = function() {
        getStaff();
    }

    $scope.prepareToAddNewStaff = function() {
        $scope.updateResult = null;
        $scope.newStaff = new StaffAPI();
    }

    $scope.prepareToDeleteStaff = function(employee) {
        $scope.updateResult = null;
        $scope.staffToDelete = employee;
    }

    $scope.addStaff = function() {
        $scope.newStaff.$save(function(response) {
            addStaffModal.modal('hide');
            getStaff();
        }, function(response) {
            $scope.updateResult = response.data;
        });
    }

    $scope.deleteStaff = function() {
        $scope.staffToDelete.$delete({ id: $scope.staffToDelete.staff_id }, function(response) {
            deleteStaffModal.modal('hide');
            getStaff();
        }, function(response) {
            $scope.updateResult = response.data;
        });
    }

    $scope.showStaffAddressOnMap = function(employee) {
        $scope.staffToShowAddressOnMap = employee;
        geocoder.geocode({
            address: employee.address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if(results.length > 0) {
                    map.setCenter(results[0].geometry.location);
                    var infoWindow = new google.maps.InfoWindow({
                        content: '<b>' + employee.address + '</b>',
                        size: new google.maps.Size(150, 50)
                    });
                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map,
                        title: employee.address
                    });
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWindow.open(map, marker);
                    });
                } else {
                    alert('Address not found!');
                }
            } else {
                console.log(status);
                alert('Address not found!');
            }
        });
    }

    getStaff();
}]);

app.controller('UsersController', ['$scope', 'UsersAPI', function($scope, UsersAPI) {
    var addUserModal = $('#modal_add_user');
    var deleteUserModal = $('#modal_delete_user');

    var getUsers = function() {
        $scope.users = UsersAPI.query();
    }

    $scope.prepareToAddNewUser = function() {
        $scope.updateResult = null;
        $scope.newUser = new UsersAPI();
    }

    $scope.prepareToDeleteUser = function(user) {
        $scope.updateResult = null;
        $scope.userToDelete = user;
    }

    $scope.addUser = function() {
        $scope.newUser.$save(function(response) {
            addUserModal.modal('hide');
            getUsers();
        }, function(response) {
            $scope.updateResult = response.data;
        });
    }

    $scope.deleteUser = function() {
        $scope.userToDelete.$delete({ id: $scope.userToDelete.ID }, function(response) {
            deleteUserModal.modal('hide');
            getUsers();
        }, function(response) {
            $scope.updateResult = response.data;
        });
    }

    getUsers();
}]);

app.controller('LogoutController', ['$rootScope', '$scope', '$state', 'LogoutAPI', function($rootScope, $scope, $state, LogoutAPI) {
    LogoutAPI.logout(function(response) {
        $rootScope.currentUser = null;
        $state.go('login');
    }, function(response) {
        console.log(response);
    });
}]);

app.factory('LoginAPI', ['$window', '$resource', function($window, $resource) {
    return $resource($window.baseURL + 'index.php/api/login', null, {
        captcha: {
            url: $window.baseURL + 'index.php/api/login/captcha',
            method: 'GET'
        },
        login: {
            method: 'POST'
        },
        logout: {
            url: $window.baseURL + 'index.php/api/login/logout',
            method: 'POST'
        }
    });
}]);

app.factory('StaffAPI', ['$window', '$resource', function($window, $resource) {
    return $resource($window.baseURL + 'index.php/api/staff');
}]);

app.factory('UsersAPI', ['$window', '$resource', function($window, $resource) {
    return $resource($window.baseURL + 'index.php/api/users');
}]);

app.factory('LogoutAPI', ['$window', '$resource', function($window, $resource) {
    return $resource($window.baseURL + 'index.php/api/logout', null, {
        logout: {
            method: 'POST'
        }
    });
}]);